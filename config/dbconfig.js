import dotenv from 'dotenv'
dotenv.config();
import { Client as _Client } from 'harperive';

const DB_CONFIG = {
    harperHost: process.env.INSTANCE_URL,
    username: process.env.INSTANCE_USERNAME,
    password: process.env.INSTANCE_PASSWORD,
    schema: process.env.INSTANCE_SCHEMA // optional
};

const Client = _Client;
const db = new Client(DB_CONFIG);

export default db;