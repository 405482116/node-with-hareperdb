import db from "../../config/dbconfig";

export const addBook = async (ctx, next) => {
    const body = ctx.request.body;
    try {
        const { statusCode, data: { message, inserted_hashes }, status } = await db.insert(
            {
                table: 'books',
                records: [
                    {
                        title: body.title,
                        author: body.author
                    }
                ]
            }
        );
        ctx.status = statusCode;
        ctx.body = {
            status,
            data: {
                message,
                id: inserted_hashes,
            }
        }
    } catch (error) {
        ctx.status = 500;
        ctx.body = {
            message: error.error,
        }
    }
};
export const getByAuthor = async (ctx, next) => {
    const body = ctx.request.body;
    const query = `SELECT * FROM dev.books WHERE author = "${body.author}"`
    //select * from dev.books where author="Jason" ORDER BY __createdtime__  desc
    //select author,id, createtime  from dev.books where id = "22478a02-7c39-4f1a-aebd-3edf45515bdc"
    try {
        // const { statusCode, data, status } = await db.searchByValue({
        //     table: 'books',
        //     searchAttribute: 'author',
        //     searchValue: body.author,
        //     attributes: ['*']
        // })
        const { statusCode, status, data } = await db.query(query);
        ctx.status = statusCode;
        ctx.body = {
            status,
            data
        }
    } catch (error) {
        console.log(111, error)
        ctx.status = 500;
        ctx.body = {
            message: error.error,
        }
    }
};

export const getById = async (ctx, next) => {
    const body = ctx.request.body;
    try {
        const { statusCode, data, status } = await db.searchByHash({
            table: 'books',
            hashValues: [body.id],
            attributes: ['title', 'author']
        });
        ctx.status = statusCode;
        ctx.body = {
            status,
            data,
        }
    } catch (error) {
        ctx.status = 500;
        ctx.body = {
            message: error.error,
        }
    }
};

export const deleteBook = async (ctx, next) => {
    const body = ctx.request.body;
    try {
        const { status, statusCode, data } = await db.delete({
            table: 'books',
            hashValues: [body.id]
        })
        ctx.status = statusCode;
        ctx.body = {
            status,
            data,
        }
    } catch (error) {
        ctx.status = 500;
        ctx.body = {
            message: error.error,
        }
    }
};