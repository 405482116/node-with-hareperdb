import Koa from "koa";
import koaBody from "koa-body";
import Router from "@koa/router";
import { addBook, getByAuthor, getById, deleteBook } from "./api";
const app = new Koa();
const router = new Router();
router.post('/books', addBook);
router.post('/author', getByAuthor);
router.post('/search', getById);
router.post('/delete', deleteBook);

const PORT = 3030;

app.use(koaBody());

app.use(router.routes())
    .use(router.allowedMethods());
app.use(async ctx => {
    ctx.body = 'Hello World';
});

app.listen(PORT, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
})